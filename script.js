"use strict";

(function () {
  "use strict";
  window.addEventListener(
    "load",
    function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName("needs-validation");
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener(
          "submit",
          function (event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add("was-validated");
          },
          false
        );
      });
    },
    false
  );
})();

const contactForm = document.querySelector("#contact-form");

contactForm.addEventListener("submit", function (e) {
  e.preventDefault();

  Swal.fire({
    title: "Sending Message . . .",
    didOpen: () => {
      Swal.showLoading();
    },
    footer: "Sending",
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
  });

  const name = document.querySelector("#name").value;
  const email = document.querySelector("#email").value;
  const contactNumber = document.querySelector("#number").value;
  const message = document.querySelector("#message").value;
  const agreed = !document.querySelector("#gridCheck").validity.valueMissing;

  if (!name || !email || !message || !agreed) return;

  fetch("https://pixiurl.herokuapp.com/api/personal-portfolio", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name, email, contactNumber, message, agreed }),
  })
    .then((result) => result.json())
    .then((result) => {
      Swal.close();
      if (result) {
        Swal.fire({
          title: "Success!",
          text: "Message Sent",
          icon: "success",
          showConfirmButton: false,
        });

        contactForm.reset();
        contactForm.classList.remove("was-validated");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Something went wrong!",
        });
      }
    });
});
